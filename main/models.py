from django.db import models

# Create your models here.
class Schedule(models.Model):
    Day= models.CharField(max_length=50)
    Date = models.DateField(blank = False)
    Time= models.TimeField()
    Name= models.TextField(max_length=50)
    Location = models.TextField(max_length=50)
    Category = models.TextField(max_length=50)

class Matkul(models.Model):
    Name= models.TextField(max_length=50)
    Sks=models.IntegerField()
    Day=models.CharField(max_length=50)
    Time= models.TimeField()
    Date= models.DateField(blank=False)

