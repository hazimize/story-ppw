from django import forms

class SchedForm(forms.Form):
    Day= forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Day',
        'type' : 'text',
        'required' : True,
    }))
    Date = forms.DateField(widget=forms.DateInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'dd/mm/yyyy',
        'type': 'date',
        'required': True,
    }))
    Time = forms.TimeField(widget=forms.TimeInput(attrs={
        'class' : 'form-control',
        'type' : 'time',
        'required': True,
    }))
    Name = forms.CharField(widget=forms.TimeInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
        'placeholder' : 'Activity Name',
        'required': True,
    }))

class MatkulForm(forms.Form):
    Name= forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Matkul',
        'type' : 'text',
        'required' : True,
    }))
    Sks= forms.IntegerField (widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Jumlah SKS',
        'type' : 'text',
        'required' : True,
    }))
    Day= forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Day',
        'type' : 'text',
        'required' : True,
    }))
    Time = forms.TimeField(widget=forms.TimeInput(attrs={
        'class' : 'form-control',
        'type' : 'time',
        'required': True,
    }))
    Date = forms.DateField(widget=forms.DateInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'dd/mm/yyyy',
        'type': 'date',
        'required': True,
    }))
   