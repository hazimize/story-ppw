from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.portofolio, name='portofolio'),
    path('blog/', views.blog, name='blog'),
    # path('Schedule/', views.Join, name='Schedule'),
     # path('<int:pk>', views.sched_delete, name='Delete'),
    path('matkul/',views.MatkulJoin, name='Matkul'),
    path('<int:pk>', views.matkul_delete, name='Delete'),
]
