from django.shortcuts import render,redirect
from .models import Schedule as jadwal, Matkul as matkul
from .forms import SchedForm, MatkulForm



def portofolio(request):
    return render(request, 'main/portofolio.html')
def blog(request):
    return render(request, 'main/blog.html')
def Join(request):
    if request.method=="POST":
        form=SchedForm(request.POST)
        if form.is_valid():
            sched= jadwal()
            sched.Day = form.cleaned_data['Day']
            sched.Date = form.cleaned_data['Date']
            sched.Time = form.cleaned_data['Time']
            sched.Name = form.cleaned_data['Name']
            # sched.Location = form.cleaned_data['Location']
            # sched.Category = form.cleaned_data['Category']
            sched.save()
        return redirect('/Schedule')
    else:
        sched = jadwal.objects.all()
        form = SchedForm()
        response = {"sched":sched, 'form':form}
        return render (request, 'main/Schedule.html',response)
def sched_delete(request,pk):
    if request.method == "POST":
        form = SchedForm(request.POST)
        if form.is_valid():
            sched= jadwal()
            sched.Day = form.cleaned_data['Day']
            sched.Date = form.cleaned_data['Date']
            sched.Time = form.cleaned_data['Time']
            sched.Name = form.cleaned_data['Name']
            # sched.Location = form.cleaned_data['Location']
            # sched.Category= form.cleaned_data['Category']
            sched.save()
        return redirect('/Schedule')
    else:
        jadwal.objects.filter(pk=pk).delete()
        data= jadwal.objects.all()
        form = SchedForm()
        response={"sched":data, 'form':form}
        return render(request, 'main/Schedule.html', response)


def MatkulJoin(request):
    if request.method=="POST":
        form=MatkulForm(request.POST)
        if form.is_valid():
            matakul= matkul()
            matakul.Name=form.cleaned_data['Name']
            matakul.Sks = form.cleaned_data['Sks']
            matakul.Day = form.cleaned_data['Day']
            matakul.Time = form.cleaned_data['Time']
            matakul.Date = form.cleaned_data['Date']
            matakul.save()
        return redirect('/matkul')
    else:
        matakul = matkul.objects.all()
        form = MatkulForm()
        response = {"matakul":matakul, 'form':form}
        return render (request, 'main/Matkul.html',response)
def matkul_delete(request,pk):
    if request.method == "POST":
        form = MatkulForm(request.POST)
        if form.is_valid():
            matakul= matkul()
            matakul.Name=form.cleaned_data['Name']
            matakul.Sks = form.cleaned_data['Sks']
            matakul.Day = form.cleaned_data['Day']
            matakul.Time = form.cleaned_data['Time']
            matakul.Date = form.cleaned_data['Date']
            matakul.save()
        return redirect('/matkul')
    else:
        matkul.objects.filter(pk=pk).delete()
        data= matkul.objects.all()
        form = MatkulForm()
        response={"matakul":data, 'form':form}
        return render(request, 'main/Matkul.html', response)