from django.apps import AppConfig


class StorypertamaConfig(AppConfig):
    name = 'storypertama'
